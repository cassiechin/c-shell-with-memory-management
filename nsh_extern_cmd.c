/*
 * File name: nsh_extern_cmd.c
 *
 * Description: Includes functions for using the where command and executing external commands
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include "nsh_extern_cmd.h"
#include "nsh_env_alias.h" /* Used to find the path */

/* 
 * Function name: where
 *
 * Description: Finds the permissions of a certain file
 *
 * Input: The directory to search
 *        The type of file to look for
 *        The name of the file to look for
 *
 * Output: The path of the file if found
 *         Null if not found
 *
 */

char* where (int type, char *filename) { 
  char *return_path = NULL; // the path the file is found at
  struct stat stats;

  if( stat( filename, &stats ) == 0 ) {
    switch (type) {
    case READ: // if they have at least one of the three then they have valid access
      if(stats.st_mode & S_IRUSR) return_path = filename; 
      if(stats.st_mode & S_IRGRP) return_path = filename; 
      if(stats.st_mode & S_IROTH) return_path = filename; 
      break;
    case WRITE:
      if(stats.st_mode & S_IWUSR) return_path = filename; 
      if(stats.st_mode & S_IWGRP) return_path = filename; 
      if(stats.st_mode & S_IWOTH) return_path = filename; 
      break;
    case RUN:
      if(stats.st_mode & S_IXUSR) return_path = filename; 
      if(stats.st_mode & S_IXGRP) return_path = filename; 
      if(stats.st_mode & S_IXOTH) return_path = filename; 
      break;
    case DIRECT:
      if(S_ISDIR(stats.st_mode)) return_path = filename; 
      break;
    default:
      break;
    }
  }
  else  {
    //      fprintf( stderr, "ERROR: stat failed on {%s}\n", absname );
  }

  return return_path;
}

/*
 * Function name: exenew
 *
 * Description: Execute an external command
 *
 * Input: the path of the executable
 *        the command to execute
 *        the command argument
 *
 * Output: None
 *
 */

void exenew (char *path, char *command, char *arg) {
  pid_t child_pid;  //pid of process that will execute external command

  /* exit if fork fails */
  if((child_pid = fork()) < 0 ) {
    perror("fork failure");
    //    exit(1); 
    return;
  }

  if(child_pid == 0){
    execl (path, command, arg, NULL);   // Child executes external command
    perror("Error!");
    printf("-nsh: %s: command not found\n", command); 

    _exit(1);
  }
  else {  // nsh waits for the external command to finish executing
    wait ();
  }
}

/*    
 * Function: parse_path
 * 
 * Definition: Takes a path and then parses the path
 *                into different parts by the seperation symbol 
 *    
 * Input: a path to be parsed 
 *
 * Output: number of parsed elements
 */

int parse_path (char *path, char *path_array[]){
  int i;
  int elements = 1; // the number of exclamation points + 1
  int counter = 0;  //counter elements 
  char *token;      //token for each path
  char *temp_path = calloc (100,1); // store the path to parse in here

  strcpy (temp_path, path);

  for (i=0; i<strlen(temp_path); i++){
    if(temp_path[i]=='!')  //finds amount of elements in path
      elements++; 
  }

  if (elements == 1) {
    path_array[0] = temp_path;
    return elements;
  }

  token=strtok(temp_path,"!");
  while(token!=NULL){ //continue until end of path
    path_array[counter++]=token;
    token= strtok(NULL,"!\0");
  }

  return elements;
}

/* 
 * Function name: where_command_ui
 *
 * Description: process the where command
 *
 * Input: the type of command to look for
 *        the name of the file to look for
 *
 * Output: The location of the file if found
 *         Null if not found
 *
 */

char * where_command_ui  (char *type, char *file) {
  int i = 0;  /* For loop counter */
  int command_code; /* Check for internal commands */
  REC *rec;         /* Check for aliases */
  char *path_ptr;   /* Check to see if a path exists*/
  int e_type = 0;   /* Represents either ead, write, run or directory*/

  /* Variables for parsing the environment variable path */
  int num_of_path_elements = 0; /* Number of elements in the path */
  char *path[MAX_PATH]; /* Array to store the paths in */

  int p = 0; /* Path counter */
  int Len;   /* Length of path*/
  char *filename; /* Store the path plus the file to be search for */
  char* r = NULL; /* Return value representing whether a file was found or not */

  if (strcmp (file, "") == 0) {
    printf("No file inputted.\n");
    return (char*) NULL;
  }

  /* Check for internal commands */
  command_code = find_command_code (file);
  switch (command_code) {
  case EXIT:
  case SET:
  case TES:
  case ALIAS:
  case SAILA:
  case MEMTEST:
  case MEMDUMP:
  case ECHO:
  case FILEIO:
  case WHERE:    
    printf("%s is an internal command\n", file);
    return (char *) NULL;
  default:
    break;
  }

  /* Check for aliases */
  rec = (REC *) nshFindAlias (file);
  if (rec != 0) {
    file = (char *) rec->value;
  }
  
  /* Check for external commands */
  /* Map type to enumerated value */
  if      (strcmp (type, "read")  == 0) e_type = READ;
  else if (strcmp (type, "write") == 0) e_type = WRITE;
  else if (strcmp (type, "run")   == 0) e_type = RUN;
  else if (strcmp (type, "dir")   == 0) e_type = DIRECT;
  else e_type = TYPE_ERROR;

  /* Return if an unrecognized type is entered */
  if (e_type == TYPE_ERROR) {
    printf("Invalid type\n");
    return NULL;
  }

  /* Search to see if there's an absolute path */
  for (i=0; i<strlen(file); i++){
    if(file[i] == '/') {  //finds absolute path
      r = where (e_type, file);
      if (r == NULL) printf("%s not found.\n", file);
      return r;
    }
  }

  /* Get the environment variable path */
  path_ptr =  nshFindPath ();
  if (path_ptr == NULL) {
    printf("path variable not found\n");
    return (char*) NULL;
  }
  else { // parse path
    num_of_path_elements = parse_path (path_ptr, path);
  }

  /* Search through all the paths */
  for (i=0; i<num_of_path_elements; i++) { 
    Len = strlen (path[p]) + strlen(file) + 2; // get the length of the file address to be searched
    filename = calloc ( Len, 1); // calloc space for the file name
    r = calloc (Len, 1);         // return path

    // get the entire address of the file
    strncpy( filename, path[p], strlen(path[p]) ); 
    strncat( filename, "/", 1 );
    strncat( filename, file, strlen(file) ); 

    // Search for the file
    r = where (e_type, filename);

    if (r != NULL) break; // break once the first instance is found
    p++; // increment path counter
  }

  if (r == NULL) printf("%s not found.\n", file);

  return r; 
}

/*
 * Function name: extern_command_ui
 *
 * Description: Process and external command
 *
 * Input: Command to execute
 *        Command argument
 *
 * Output: none
 *
 */

void extern_command_ui (char *cmd, char *arg) {
  char* path = NULL; // The path of the command
  int Len;
  REC *rec;

  /* Find the command executable in the bin folder */
  Len = strlen (cmd) + strlen ("/bin/") + 1;
  path = calloc (Len, 1);
  strcpy (path, "/bin/");
  strcat (path, cmd);
  path = where (RUN, path);

  /* Executable is not found in bin */
  if (path == NULL) {
    printf("command %s not found\n", cmd);
    return;
  }

  /* Else execute command */
  exenew (path, cmd, arg);
}
