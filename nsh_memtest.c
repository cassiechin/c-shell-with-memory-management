/*
 * File name: nsh_memtest.c
 *
 * Description: Includes functions to test to see if the memory subsystem works
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

// include the file declaring the memory subsytem data structure and it's functions
#include "nsh_malloc.h" 

/* Function name: memtest1
 *
 * Description: Tests the memory subsystem by mallocing space into each buffer
 *
 * Input: none
 * 
 * Output: return 0
 */

int memtest1 (void) {
  char *b1, *b2, *b3, *b4, *b5, *b6, *b7,*b8; 

  dump_mcb(); // print out the memory subsystem carrots

  b1 = nshMalloc (32);   // nshMalloc 32 bytes
  b2 = nshMalloc (64);   // nshMalloc 64 bytes
  b3 = nshMalloc (128);  // nshMalloc 128 bytes
  b4 = nshMalloc (256);  // nshMalloc 256 bytes
  b5 = nshMalloc (512);  // nshMalloc 512 bytes
  b6 = nshMalloc (1024); // nshMalloc 1024 bytes
  b7 = nshMalloc (2048); // nshMalloc 2048 bytes
  b8 = nshMalloc (4096); // nshMalloc 4096 bytes

  printf("\n"); // print a new line
  printf("The pointer that returns is 4 bytes after the buffer header\n"); // print
  printf("b1 = nshMalloc (32)   = %p\n", b1);  // print 
  printf("b2 = nshMalloc (64)   = %p\n", b2);  // print 
  printf("b3 = nshMalloc (128)  = %p\n", b3);  // print 
  printf("b4 = nshMalloc (256)  = %p\n", b4);  // print 
  printf("b5 = nshMalloc (512)  = %p\n", b5);  // print 
  printf("b6 = nshMalloc (1024) = %p\n", b6);  // print 
  printf("b7 = nshMalloc (2048) = %p\n", b7);  // print 
  printf("b8 = nshMalloc (4096) = %p\n", b8);  // print 
  printf("\n"); // \n means a new line

  dump_mcb(); // print out the memory subsystem

  nshFree (b1); // free b1
  nshFree (b2); // free b2
  nshFree (b3); // free b3
  nshFree (b4); // free b4
  nshFree (b5); // free b5
  nshFree (b6); // free b6
  nshFree (b7); // free b7
  nshFree (b8); // free b8

  return 0;
}

/* Function name: memtest2
 *
 * Description: Tests the memory subsystem by mallocing and callocing space into each buffer
 *
 * Input: none
 * 
 * Output: return 0
 */

int memtest2 (void) {
  char *a;
  char *b;
  char *c;

  dump_mcb(); // print out the memory subsystem

  a = nshMalloc (500);    // malloc 500
  b = nshCalloc (2, 200); // calloc 200
  nshFree (b);            // free b
  c = nshMalloc (450);    // malloc more space! (450 bytes)

  printf("\n");
  printf("a = nshMalloc (500)    = %p\n", a); // this is a print statement
  printf("b = nshCalloc (2, 200) = %p\n", b); // this is also a print statment
  printf("nshFree (b)\n");                    // wow, a third print statment
  printf("c = nshMalloc (450)    = %p\n", c); // print "c = nshMalloc (450)    = whatever the pointer is"
  printf("\n");                               // we even have a print statment for a new line

  dump_mcb(); // print out the memory subsystem

  nshFree (a); // reset the memory subsystem
  nshFree (c); // reset the memory subsystem

  return 0;
}

/* Function name: memtest3
 *
 * Description: Tests the memory subsystem by mallocing and reallocing space into each buffer
 *
 * Input: none
 * 
 * Output: return 0
 */

int memtest3 (void) {
  char *a;
  char *b;

  dump_mcb(); // print out the memory subsystem

  a = nshMalloc (1000);     // malloc 1000 bytes of space 
  b = nshRealloc (a, 3000); // realloc 3000 bytes of space

  printf("\n"); 
  printf("a = nshMalloc (1000)     = %p\n", a); // print statement
  printf("b = nshRealloc (a, 3000) = %p\n", b); // print statement
  printf("\n");
 
  dump_mcb(); // print out the memory subsystem

  return 0;
}
