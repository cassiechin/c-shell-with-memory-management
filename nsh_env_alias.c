/*
 * File name: nsh_env_alias.c
 *
 * Description: Includes functions that maintain two records.
 *              The first is a list of environment variable records.
 *              The second is a list of alias records.
 *              File has the ability to insert, delete, sort and return records.
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include "nsh_env_alias.h"

static REC *list_env   = NULL; // pointer to the first environment variable in the list
static REC *list_alias = NULL; // pointer to the first alias in the list
static REC *next_env   = NULL; // used for nshNext
static REC *next_alias = NULL; // used for nshNext

/* Function:   insert_rec_into_list
 * 
 * Description: inserts a record into the singly linked list
 *              in the correct alphabetic spot
 *
 * Input:       A pointer to the pointer of the first element in the
 *              list and a pointer to the new record
 *
 * Output:      none
 */

static int insert_rec_into_list(REC  **list, REC *rec) {
  REC *p_list;

  if (*list == NULL) { // if list is NULL then there is no list
    *list = rec; // set the address of the list element to the record
  }
  else {
    p_list = *list; // set p_list to the address of the first element in the list

    while (p_list!= NULL) {
      if (strcmp(rec->variable, (*list)->variable) < 0) { // Special case: variable is less than the head
	rec->next = *list;  // set the new_env next pointer to point to the current head
	*list = rec; // move the head pointer to new env
	break;
      }
      
      if ((p_list->next == NULL) && (strcmp (rec->variable, p_list->variable) > 0)) { // Special case: variable is greater than last element
	p_list->next = rec;
	rec->next = NULL;
	break;    
      }

      // nshUpdate
      if (strcmp (rec->variable, p_list->variable) == 0) {  // Special case:variable already exists so update this variable
	strcpy (p_list->value, rec->value);
	nshFree (rec);
	break;
      }

      // Insert an element into the list
      if ((strcmp (rec->variable, p_list->variable) > 0) && (strcmp (rec->variable, p_list->next->variable) < 0)) { 
	rec->next = p_list->next; // set the next pointer of the new record to the next pointer of the list pointer->next element
	p_list->next = rec;           // set the next pointer of the list pointer->next to equal the new record
	break;
      } 

      p_list = p_list->next; // increment pointer to list
    } // end for
  } // end else
}

/* Function: nshInsert
 * 
 * Description: Allocates the space and places a record into memory
 *
 * Input: two pointers to the variable and value
 *        and an integer referending which list is used
 *
 * Output: returns 1 if insertion was successful
 */

int nshInsert(char *variable, char *value, int list) {
  char *ptr;
  REC  *rec;
  REC *insert;
  int  cmd_code;

  rec = (REC *)nshCalloc(1, sizeof(REC)); // calloc space for a new record
  if (rec == NULL) {
    return 0;
  }

  strcpy(rec->variable, variable); // set the variable and value
  strcpy(rec->value, value);

  switch (list) {
  case L_ENV:
    rec->command_code = 0; // if the record is an environment variable the command code is 0
    insert_rec_into_list(&list_env, rec); 
    break;

  case L_ALIAS:
    cmd_code = find_command_code (value); 
    rec->command_code = cmd_code;  // set the command code of an alias
    insert_rec_into_list(&list_alias, rec);
    break;
  }

  return 1; // return true if insert was successful
}

/* Function: nshRemove 
 * 
 * Description: Function that removes a variable from memory and the list 
 *
 * Input: pointer of a variable
 *
 * Output: none
 */

void nshRemove (char *variable, int list) {
  char *ptr;
  REC  *rec_before_delete;
  REC  *rec_to_delete;

  switch(list) {
  case L_ENV:
    if (strcmp (variable, list_env->variable) == 0) { // Test to see if variable is the first variable in the env list
      rec_to_delete = list_env;        // set the record to delete as the head of the env list
      list_env = rec_to_delete->next;  // set the head of the list to the second element in the list (we're deleting the first element)
      ptr = (char *) rec_to_delete;    // nshFree takes a char* pointer, so cast the record to delete as a char* pointer
      nshFree (ptr);                   // free the record to delete
      break;
    }
    ptr = nshFind (&list_env, variable);  // If the variable is not the first element, search through the list
                                          // ptr is the record BEFORE the record to delete
    if (ptr == 0) break;                  // break if the variable was not found
    rec_before_delete = (REC *) ptr;      // cast the record before delete as a record
    rec_to_delete = rec_before_delete->next; // set the record to delete (the nshFind returns a pointer to the record BEFORE delete)
    rec_before_delete->next = rec_to_delete->next; // set the next-pointer of the record before delete to the record after the deleted record
    ptr = (char *) rec_to_delete;         // cast the record to delete as a char* pointer
    nshFree (rec_to_delete);              // free the record to delete
    break;

  case L_ALIAS:
    if (strcmp (variable, list_alias->variable) == 0) {
      rec_to_delete = list_alias;
      list_alias = rec_to_delete->next;
      ptr = (char *) rec_to_delete;
      nshFree (ptr);
      break;
    }
    ptr = nshFind (&list_alias, variable); 
    if (ptr == 0) break;
    rec_before_delete = (REC *) ptr;
    rec_to_delete = rec_before_delete->next;
    rec_before_delete->next = rec_to_delete->next;
    ptr = (char *) rec_to_delete;
    nshFree (rec_to_delete);
    break;
  }
}

/* Function: nshFind
 * 
 * Description: finds the address of the pointer directly
 *              before a variable
 * Input: A pointer to the variable and a pointer to the
 *        beginning of the list
 *
 * Output: A pointer to the element directly before the variable 
 */

char * nshFind(REC **list, char *variable) {
  char *r_pointer;
  REC  *ptr;

  ptr = *list;

  // Special case: There is only one variable in the list

  if (ptr->next == 0) {
    return 0;  // nshFind returns the address of the record before the variable
  }

  while (ptr != NULL && ptr->next != NULL) {
    if (strcmp(variable, ptr->next->variable) == 0) { // compare the value of the NEXT record
      r_pointer = (char *) ptr;  
      return r_pointer;  // return the address of the record BEFORE the record found
    }
   ptr = ptr->next;
  }

  return 0;
}


/* Function: nshFindAlias
 * 
 * Description: finds the address of the pointer directly
 *              before a variable
 * Input: A pointer to the variable and a pointer to the
 *        beginning of the list
 *
 * Output: A pointer to the element directly before the variable 
 */

char * nshFindAlias (char *variable) {
  char *r_pointer;
  REC  *ptr;

  ptr = list_alias;

  while (ptr != NULL) {
    if (strcmp(variable, ptr->variable) == 0) { 
      r_pointer = (char *) ptr;  
      return r_pointer;  
    }
   ptr = ptr->next;
  }

  return 0;
}

/* Function: nshFindEnv
 *
 * Description: finds the address of the pointer directly
 *              before a variable
 *
 * Input: A pointer to the variable and a pointer to the
 *        beginning of the list
 *
 * Output: A pointer to the element directly before the variable
 */

char * nshFindEnv (char *variable) {
  char *r_pointer;
  REC  *ptr;

  ptr = list_env;

  while (ptr != NULL) {
    if (strcmp(variable, ptr->variable) == 0) { 
      r_pointer = (char *) ptr;  
      return r_pointer;  
    }
   ptr = ptr->next;
  }

  return 0;
}

/* Function: nshFindPath
 *
 * Description: Looks for the path environment variable
 *
 * Input: None
 *
 * Output: A character pointer to the path record
 */

char * nshFindPath () {
  char *p_temp;
  REC *p_rec;
  char *path_ptr;

  p_temp =  nshFindEnv ("path");
  if (p_temp == 0) return NULL;
  p_rec = (REC *) p_temp;
  path_ptr = p_rec->value;
  return path_ptr;
}

/* Function: nshHead        
 * 
 * Description: Places the "next" pointer to point to the beggining
 *              of the list and calls the next function
 *
 * Input: an integer referencing which list to use
 *
 * Output: prints the head variable and value 
 */

void nshHead(int list) {
  switch(list) {
  case L_ENV:
    next_env=list_env;
  case L_ALIAS:
    next_alias=list_alias;
  }
  nshNext(list);
}

/* Function: nshNext
 * 
 * Description: Prints the next variable and moves the next pointer
 *
 * Input: An integer refernecing the list to use
 *
 * Output: prints the next variable and value 
 */

void nshNext(int list) {
  switch(list){
  case L_ENV:
    if(next_env==NULL)
      printf("There are no more enviromental variables.\n");
    else { 
      printf ("%s = %s\n", next_env->variable, next_env->value);
      next_env=next_env->next;
    }
    break;

  case L_ALIAS:
    if(next_alias==NULL)
      printf("There are no more aliases.\n");
    else { 
      printf ("%s='%s'\n", next_alias->variable, next_alias->value);
      next_alias=next_alias->next;
    }
    break;
  }
}

/* Function: nshDisplayList
 * 
 * Description: Displays the entire list 
 *
 * Input: Integer representing which list to use
 *
 * Output: prints the list 
 */

void nshDisplayList(int list) {
  REC *ptr;

  switch (list) {
  case L_ENV:
    ptr = list_env;
    while (ptr != NULL) {
      printf("%s= %s\n", ptr->variable, ptr->value);
      ptr = ptr->next;
    }
    break;

  case L_ALIAS:
    ptr = list_alias;
    while (ptr != NULL) {
      printf("%s='%s'\n", ptr->variable, ptr->value);
      ptr = ptr->next;
    }
    break;
  }

}

/* Function: nshDisplayRec
 * 
 * Description: displays a record by calling the nshFind function
 *
 * Input: pointer to the variable and integer for the list to use
 *
 * Output: prints the record.
 */

void nshDisplayRec(char* variable, int list) {
  char *ptr;
  REC  *p_rec;

  switch (list) {
  case L_ENV:
    // Special case: there are no elements in the list
    if (list_env == NULL) {
      printf("-nsh: nshDisplayRec: %s not found\n", variable);
      return;
    }

    if (strcmp (variable, list_env->variable) == 0) { // Special case: display the first element in list
      printf("%s= %s\n", list_env->variable, list_env->value);
      break;
    }
    ptr = nshFind(&list_env, variable); // this returns the pointer to the variable before
   
    if (ptr == 0) {
      break;
    }

    p_rec = (REC *) ptr; 
    p_rec = p_rec->next;
    printf("%s= %s\n", p_rec->variable, p_rec->value);
    break;

  case L_ALIAS:
    // Special case: there are no elements in the list
    if (list_alias == NULL) {
      printf("bash: nshDisplayRec: %s not found\n", variable);
      return;
    }

    if (strcmp (variable, list_alias->variable) == 0) { // Special case: display the first element in list
      printf("%s='%s'\n", list_alias->variable, list_alias->value);
      break;
    }
    ptr = nshFind(&list_alias, variable); // this returns the pointer to the variable before

    if (ptr == 0) {
      break;
    }
    p_rec = (REC *) ptr; 
    p_rec = p_rec->next;
    printf("%s='%s'\n", p_rec->variable, p_rec->value);
    break;
  }
}

/* Function: int_command_code
 * 
 * Description: Used to express which command to execute
 *
 * Input: pointer to the command typed
 *
 * Output: integer representing a command 
 */

int find_command_code (char *cmd) {
  REC *ptr;

  // Test for internal commands
  if ((strcmp(cmd, "exit")    == 0)) return EXIT;
  if ((strcmp(cmd, "set")     == 0)) return SET;
  if ((strcmp(cmd, "tes")     == 0)) return TES;
  if ((strcmp(cmd, "alias")   == 0)) return ALIAS;
  if ((strcmp(cmd, "saila")   == 0)) return SAILA;
  if ((strcmp(cmd, "memtest") == 0)) return MEMTEST;
  if ((strcmp(cmd, "head")    == 0)) return HEAD;
  if ((strcmp(cmd, "next")    == 0)) return NEXT;
  if ((strcmp(cmd, "memdump") == 0)) return MEMDUMP;  
  if ((strcmp(cmd, "echo")    == 0)) return ECHO;
  if ((strcmp(cmd, "file")    == 0)) return FILEIO;
  if ((strcmp(cmd, "where")    == 0)) return WHERE;

  ptr = list_alias;

  while (ptr != NULL) {
    if (strcmp (cmd, ptr->variable) == 0) {
      return ptr->command_code;
    }
    ptr = ptr->next;
  }

  return ERROR;
}

/*
 * Function name: parse_env_buf
 *
 * Description: Removes the first part of a buffer until the ! is reached
 *              Remove that string from the buffer
 *
 * Input:      The string to be parced
 *             A variable to save the string
 *
 * Output:     None      
 *
 */

int parse_env_buf (char *t_string, char *value) {
  char buf[100];
  int i = 0;

  strcpy (buf, value); // store input value into buffer

  for (i=0; i<=strlen(buf); i++) {
    if (buf[i] == '!') {             // search for !
      buf[i] = '\0';                 // remove !
      strcpy (t_string, buf);        // copy buffer into t_string
      memcpy (value, &(value[i+1]), sizeof(buf)); // remove t_string from value
      return 1;
    }
  }
  strcpy (t_string, buf); // copy the buffer into t_string if there's no !

  return 0;
}

/*
 * Function name: nshExtend
 *
 * Description: Extend an environment variable by prepending or appending a env variable's value
 *
 * Input: Variable to extend
 *        Value that contains an @ sign followed by a variable name
 *        The variable and value are separated by an ! exclamation mark
 *
 * Output: None
 *
 */

void nshExtend (char *variable, char *value) {
  char t_string  [100];
  char new_value [100] = "";
  int status = 1;
  int at_status;
  char *ptr;
  REC *p_rec;

  do {
    status = parse_env_buf (t_string, value);

    // test to see if t_string has an @
    at_status = test_for_at (t_string);
    if (at_status == 1) { // CONTAINS AN @
      //remove @ sign
      strcpy (t_string, &(t_string[1])); // remove t_string from value

      ptr = nshFindEnv (t_string);
      if (ptr == NULL) return;
      p_rec = (REC*) ptr;
      strcat (new_value, p_rec->next->value);
    }
    else { // DOESN'T CONTAIN AN AT SO CPY DIRECTLY
      strcat (new_value, t_string);
    }

    if (status == 1) {
      strcat (new_value, "!");
    }

  } while (status == 1);

  // Check to see if ADDRESS of variable to extend is the first variable
  if (strcmp (variable, list_env->variable) == 0) {
    strcpy(list_env->value, new_value);
  }
  else {
    ptr = nshFind (&list_env, variable); // Get the ADDRESS of variable to extend    

    if (ptr == 0) { // create new variable
      nshInsert (variable, new_value, L_ENV);
    }
    else {
      p_rec = (REC *) ptr;
      // else replace value
      strcpy(p_rec->next->value, new_value);    
    }
  }
}
