/*
 * File name: nsh_malloc.c
 *
 * Description: Includes functions for managing a memory subsystem
 *              Functions include initializing and printing out the subsystem
 *              malloc, calloc and realloc functions
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define MAX_BUCKETS 8     // Number of buckets in Memory Control Block
#define MAX_BUFFERS 1000  // Number of buffers in each bucket

enum bucket_size {
  BUCKET_1 = 32,    // 2^5 Size of a buffer in bucket            
  BUCKET_2 = 64,    // 2^6
  BUCKET_3 = 128,   // 2^7
  BUCKET_4 = 256,
  BUCKET_5 = 512,
  BUCKET_6 = 1024,
  BUCKET_7 = 2048,
  BUCKET_8 = 4096,
};

enum buffer_flag {
  FREE   = 0,
  IN_USE = 1,
};

typedef struct {        // 4 byte buffer header that goes in front of each buffer
  short int flag;       // Indicate whether buffer is in use or free
  short int byte_count; // Indicate how many bytes are in use
} BUFFER_HEADER;

typedef struct {
  char *p_first; // Pointer to first buffer header in bucket
  char *p_last;  // Pointer to last buffer header in bucket
  int bcb_size;  // The size of each buffer
  int bcb_in_use;          // The number of buffers in use
  int bcb_free;            // The number of free buffers
  unsigned int bcb_stats_allocated; // The number of times malloc is called
  unsigned int bcb_stats_free;      // The number of times free is called
  unsigned int bcb_stats_realloc;   // The number of times realloc is called
} BCB; // Bucket Control Block

typedef struct {
  BCB buckets [MAX_BUCKETS];
} MCB; // Memory Control Block


/* Function name: nsh_malloc_init
 *
 * Description:   initialize mcb, malloc all memory
 *
 * Input:         none
 *
 * Output:        0 if memory failed to malloc
 *                1 if memory is properly allocated and mcb is initialized
 */

int nsh_malloc_init (void);

/*
 * Function name: dump_mcb
 *
 * Description: Prints the memory control blocks for debugging purposes
 *
 * Input:       Void
 *
 * Output:      Prints Pointer addresses, number of buffers in use per bucket,
 *              size of each buffer, number of free buffers, number of times
 *              malloc is called, number of times free is called, the buffer flags,
 *              and the buffer byte count.
 */

void dump_mcb (void);

/*
 * Function name: find_bucket_num_by_size
 *
 * Description: Finds the correct bucket based on size
 *
 *
 * Input: An integer representing the number of bytes needed
 *
 * Output: Returns the bucket index
 *
 */

int find_bucket_num_by_size (int);

/*
 * Function name: find_bucket_num_by_address
 *
 * Description: Use strtok to parse an input buffer
 *              A command, a name and a value are separated by spaces
 *              The value is followed by a new line
 *
 * Input: A pointer that points to the beginning of the bucket
 *
 * Output: Returns the index for the bucket address
 *
 */

int find_bucket_num_by_address (char*);

/*
 * Function name:   nsh_malloc
 *
 * Description:     Allocates the next available buffer based on
 *                  storage space(bucket index) and the first free
 *                  buffer based on the flag int the header
 *
 * Input:           An integer representing byte size
 *
 * Output:          A pointer to the newly malloc'd buffer
 */

char* nshMalloc (int);

/*
 * Function name:   nsh_calloc
 *
 * Description:     Allocates memory with all values set to zero
 *
 * Input:           An integer num and size
 *
 * Output:          Returns a pointer to the calloc'd buffer
 *
 */

char* nshCalloc (int, int);

/*
 * Function name:   nsh_realloc
 *
 * Description:     Reallocates a buffer by stoage size
 *                  checks if the buffer needs to be moved
 *                  to a higher stoage bucket and frees the current
 *                  buffer in that case
 *
 * Input:           A pointer to the beginning of the bucket and
 *                  an integer representing the size needed for the
 *                  reallocation
 *
 * Output:          Returns a pointer the reallocated buffer
 *
 */

char* nshRealloc  (char*, int);

/*
 * Function name:   nsh_free
 *
 * Description:     Function that changes the flag of a buffer
 *                  to FREE
 *
 * Input:           A pointer to the beginning of the buffer storage
 *                  directly after the header
 *
 * Output:          Void
 *
 */

void nshFree (char*);

/*
 * Function:     memdump
 *
 * Description:  Outputs the specified buffer in hex dump format.
 *
 * Input:        msg     - message string to display
 *               buf     - Source buffer
 *               len     - Source buffer size
 *
 * Output:       None
 *
 */

void memdump (char*, char*, int);
