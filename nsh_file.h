/*
 * File name: nsh_file.h
 *
 * Description: Includes functions necessary for executing file commands
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#define MAX_ENTIRE_LINE 100
#define MAX_FILE_LINE 100
#define MAX_ENTIRE_INPUT 300


/*  Function Name:  parse_file_lines
 *
 *  Description:    Parse a line from a file for execution by removing comments,
 *                  removing all unecessary spaces, and using the '?' as a command seperator,
 *                  Lastly calls the process_buf() function that handles our shell command
 *                  execution.
 *
 *  Input:          A character pointer to the input stream from the file
 *
 *  Output:         Executes the respective shell commands
 *
 */

void parse_file_lines (char *string);

/*  Function Name:  remove_at_sign
 *
 *  Description:    Removes the '@' symbol from the input string and places
 *                  the corresponding variable value in place of the variable
 *                  name after the '@' symbol
 *
 *  Input:          A pointer to the command input string
 *
 *  Output:         Changes the string for execution (if the '@' symbol is present)
 *
 */

void remove_at_sign (char * string);

/*  Function Name:  remove_dollar_sign
 *
 *  Description:    Replaces the '$' symbol in the command string to the null
 *                  character
 *
 *
 *  Input:          Pointer to the command string
 *
 *  Output:         Returns 1 if dollar sign was found so the program knows
 *                  it is a multi line command
 *                  Returns 0 if no dollar sign was found
 *
 */

int remove_dollar_sign (char * string);

/*  Function Name:  execute_resource_file
 *
 *  Description:    Opens and executes the resource "/.nshrc" file found in the home
 *                  directory. Calls the remove_dollar_sign, remove_at_sign, and parse_file_string
 *                  functions to properly parse and execute the commands in the file. Closes
 *
 *  Input:          None
 *
 *  Output:         Returns 1 if the file DNE, 0 if the file does exist
 *
 */

int execute_resource_file (void);

