/*******************************************************************************
 *
 * Program 5: Program Management and External Commands
 * Due Date : December 8, 2013
 *
 *******************************************************************************
 *
 * New Shell written by CSCE 3600 Group 16
 *
 *******************************************************************************
 *
 * Group 16 Members:
 *
 * Name         : Cassie Chin
 * Student ID # : 10807346
 * Email        : cassiechin9793@gmail.com
 *
 * Name         : Jordan Yankovich
 * Student ID # : 10818703
 * Email        : jordanyankovich@my.unt.edu
 *
 *******************************************************************************
 *
 * Date of Last Modification: December 7, 2013
 *
 *******************************************************************************
 *
 * Files and Functions Included in New Shell:
 *
 * nsh2_g16.c
 * - contains the main function
 * - calls the shell function
 *
 * nsh_util.c
 * - contains a function to parse an input buffer into 3 parts
 * - contains the shell function which initializes memory subsystem and 
 *   processes commands
 *
 * nsh_env_alias.h
 * - defines a record (can either be an env variable or an alias)
 * - defines enum commands, error, and list type
 * - defines max index for (buff, cmd, name, value)
 * - lists util functions
 * - lists insert, remove, display, find and update functions
 *
 * nsh_env_alias.c
 * - function to insert, remove, display environment variables or aliases
 * - function to find the command code of an alias
 * - function to parse env value and extend env variable
 *
 * nsh_malloc.h
 * - defines max buckets and max buffers for memory subsystem
 * - defines bucket size for all buckets
 * - define buffer_flag definition
 * - defines buffer header
 * - defines bucket control block, and buffer control block
 * - lists function to initialize memory subsystem
 * - lists nshMalloc, nshCalloc, nshRealloc, nshFree functions
 * - lists debugging functions
 *
 * nsh_malloc.c
 * - statically declare a memory control block
 * - function to initialize memory subsystem
 * - function to retrive bucket index
 * - malloc, calloc, realloc, free functions
 * - function to print out memory control block
 * - memdump function to dump out bytes at an address
 *
 * nsh_memtest.c
 * - malloc test cases for nshMalloc, nshCalloc and nshRealloc
 *
 * nsh_file.c
 * - function to execute a resource file
 * - function to remove the @ sign in a file line
 * - function to remove the $ sign in a file line
 * - function to parse each file line by ?
 *
 * nsh_extern_cmd.c
 * - functions to find the location of a file
 * - functions to execute an external command
 *
 *******************************************************************************/

int main (int argc, char *argv[]) {
  shell(); /* in nsh_util.c */
  return 0;
}
