# Comments

# result: dependencies
# TAB --> Operations

a.out:  nsh5_g16.o nsh_env_alias.o nsh_malloc.o nsh_memtest.o nsh_util.o nsh_file.o nsh_extern_cmd.o
	gcc -o nsh *.o

nsh5_g16.o: nsh5_g16.c
	gcc -c nsh5_g16.c

nsh_env_alias.o: nsh_env_alias.c nsh_env_alias.h
	gcc -c nsh_env_alias.c

nsh_malloc.o: nsh_malloc.c nsh_malloc.h
	gcc -c nsh_malloc.c

nsh_memtest.o: nsh_memtest.c nsh_malloc.h
	gcc -c nsh_memtest.c

nsh_util.o: nsh_util.c nsh_env_alias.h nsh_malloc.h
	gcc -c nsh_util.c

nsh_file.o: nsh_file.c nsh_env_alias.h
	gcc -c nsh_file.c

nsh_extern_cmd.o: nsh_extern_cmd.c nsh_extern_cmd.h nsh_env_alias.h
	gcc -c nsh_extern_cmd.c

clean:
	/bin/rm *.o

run :
	./nsh

all:    ./nsh
