/*
 * File name: nsh_file.c
 *
 * Description: Includes functions necessary for executing file commands
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include "nsh_env_alias.h"

/*  Function Name:  parse_file_lines
 *
 *  Description:    Parse a line from a file for execution by removing comments,
 *                  removing all unecessary spaces, and using the '?' as a command seperator,
 *                  Lastly calls the process_buf() function that handles our shell command 
 *                  execution.
 *
 *  Input:          A character pointer to the input stream from the file 
 *
 *  Output:         Executes the respective shell commands
 *
 */

void parse_file_lines (char *string) {
  int i=0; // counter to go through string
  char array [10][MAX_BUFF_INDEX]; // array of strings to be processed
  int j=0; // string number
  int k=0; // index of each string in the array

  memset (array, 0, sizeof(array));

  for (i=0; i<strlen(string) ; i++) { // proccess every character besides the null terminator
    if (string[i] == '@') {

    }
    if (string[i] == '~') { // comment character found
      do {
	array[j][k--] = '\0'; // remove all spaces before the comment line
      } while (array[j][k] == ' '); 
      if (k==-1) j--; // there is NO input before the comment line
      break; // ignore the rest of the line 
    }
    if (string[i] == '?') {
      do {  
	array[j][k--] = '\0'; // delete all spaces before the ? mark
      } while (array[j][k] == ' ');
      while (string[i+1] == ' ') i++; // move the i counter over all of the spaces after the ? mark
      j++; // go to next line in the array table
      k = 0; // set the index back to zero for the next string
    }
    else {
      array[j][k] = string[i];
      k++;
    }
  }

#if 0
  for (i=0; i<=j; i++){
    printf("<%s>\n", array[i]); // Use each string in this array as input to the shell
  }
#endif 

  for (i=0; i<=j; i++) {
    proccess_buf (array[i]);
  }

  return;
}

/*  Function Name:  remove_at_sign
 *
 *  Description:    Removes the '@' symbol from the input string and places
 *                  the corresponding variable value in place of the variable 
 *                  name after the '@' symbol 
 *
 *  Input:          A pointer to the command input string 
 *
 *  Output:         Changes the string for execution (if the '@' symbol is present) 
 *
 */

void remove_at_sign (char * string) {
  int i;
  char temp[MAX_BUFF_INDEX] = "";  int t=0;
  char variable [MAX_NAME_INDEX]; int v=0;
  char *ptr;
  REC *p_rec;

  for (i=0; i<strlen(string); i++) {
    v = 0;
    if (string[i] == '@') {
      for (i=i+1;i<strlen(string);i++) {
	if (string[i] == '\n') {
	  variable[v] = '\0';
	  ptr = (char *) nshFindEnv (variable);
	  if (ptr == NULL) return;
	  p_rec = (REC *) ptr;
	  strcat (temp, p_rec->value);
	  strncpy (string, temp, strlen(temp));
	  t += strlen (p_rec->value);
	  strcpy (string, temp);
	  return;
	}
	else if (string[i] == '!') {
	  variable[v] = '\0';
	  ptr = (char *) nshFindEnv (variable);
	  p_rec = (REC *) ptr;
	  strcat (temp, p_rec->value); t += strlen (p_rec->value);
	  strcat (temp, "!");          t += 1;
	  break;
	}
	else {
	  variable[v++] = string[i];
	}
      }
    }
    else {
      temp [t++] = string[i];
    }
  }
  strcpy (string, temp);
}


/*  Function Name:  remove_dollar_sign
 *
 *  Description:    Replaces the '$' symbol in the command string to the null 
 *                  character 
 *                 
 *
 *  Input:          Pointer to the command string 
 *
 *  Output:         Returns 1 if dollar sign was found so the program knows
 *                  it is a multi line command 
 *                  Returns 0 if no dollar sign was found 
 *
 */

int remove_dollar_sign (char *string) {
  int i;
  for (i=0; i<strlen(string); i++) {
    if (string[i] == '$') {
      string[i] = '\0';
      return 1; // a dollar sign was found
    }
  }
  return 0; // no dollar sign was found
}

/*  Function Name:  execute_resource_file
 *
 *  Description:    Opens and executes the resource "/.nshrc" file found in the home
 *                  directory. Calls the remove_dollar_sign, remove_at_sign, and parse_file_string
 *                  functions to properly parse and execute the commands in the file. Closes 
 *
 *  Input:          None 
 *
 *  Output:         Returns 1 if the file DNE, 0 if the file does exist 
 *
 */

int execute_resource_file (char *file) {
  FILE *fp;          // location of file

  char single_line [MAX_BUFF_INDEX];                // store each individual line here
  char entire_input [MAX_ENTIRE_INPUT] = ""; // input with concatenation ($)
  int dollar_return = 0;        // 0 if there IS a dollar sign

  fp = fopen (file, "r");              // open the file for read only
  if (fp == NULL) return 0;            // return if file does not exist

  while (fgets (single_line, sizeof(single_line), fp)) { // read in 1 line of data
    if (single_line[0] == '\n') continue;               // ignore empty lines
    dollar_return = remove_dollar_sign (single_line);   // remove the dollar sign
    remove_at_sign (single_line);                       // replace @ sign variables with their values
    strcat (entire_input, single_line);                 // add the input string to the string to be parsed
    if (dollar_return == 0) {                  // no dollar sign was found
      parse_file_lines (entire_input);         // parse the string
      memset (entire_input, 0, strlen (entire_input)); // reset the input buffer
    }
  }
  fclose (fp); // Close the file

  return 1;
}
