/*
 * File name: nsh_malloc.c
 *
 * Description: Includes functions for managing a memory subsystem
 *              Functions include initializing and printing out the subsystem
 *              malloc, calloc and realloc functions
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include "nsh_malloc.h"

MCB mcb;

/* Function name: nsh_malloc_init 
 *
 * Description:   initialize mcb, malloc all memory 
 *
 * Input:         none
 *
 * Output:        0 if memory failed to malloc
 *                1 if memory is properly allocated and mcb is initialized 
 */

int nsh_malloc_init (void) {
  int i;
  int buffer_size = BUCKET_1;
  int memory_pool_size = 0;
  char *p;

  // Get size of memory
  for (i=0; i<MAX_BUCKETS; i++) {
    memory_pool_size += sizeof(BUFFER_HEADER) + buffer_size;
    buffer_size *= 2;
  }

  // Calloc memory
  p = calloc (MAX_BUFFERS, memory_pool_size);
  if (p==NULL) {
    return 0;
  }

  // Initialize variables in the buffer
  buffer_size = BUCKET_1; // Reset buffer size to 32
  for (i=0; i<MAX_BUCKETS; i++) {
    mcb.buckets[i].p_first             = p;
    mcb.buckets[i].p_last              = p + ((buffer_size + sizeof(BUFFER_HEADER)) * (MAX_BUFFERS-1));
    mcb.buckets[i].bcb_size            = buffer_size;
    mcb.buckets[i].bcb_in_use          = 0;
    mcb.buckets[i].bcb_free            = MAX_BUFFERS;
    mcb.buckets[i].bcb_stats_allocated = 0;
    mcb.buckets[i].bcb_stats_free      = 0;

    // Calculate size of bucket
    memory_pool_size = sizeof(BUFFER_HEADER) + buffer_size;
    memory_pool_size *= MAX_BUFFERS; 

    p += memory_pool_size; // Increment pointer by sizeof bucket
    buffer_size *= 2;      // Multiply size of buffer by 2
  }

  return 1;
}

/*
 * Function name: dump_mcb
 *
 * Description: Prints the memory control blocks for debugging purposes 
 *
 * Input:       Void
 * 
 * Output:      Prints Pointer addresses, number of buffers in use per bucket,
 *              size of each buffer, number of free buffers, number of times
 *              malloc is called, number of times free is called, the buffer flags,
 *              and the buffer byte count.
 */

void dump_mcb (void) {
  int i;
  int j;
  char *p;
  int buffer_size = BUCKET_1;
  BUFFER_HEADER *p_buff;

  printf ("BCB\tsize\tp_first\t\tp_last\t\tin_use\tfree\ta_alloc\ts_free\trealloc\n");
  for (i=0; i<MAX_BUCKETS; i++) {
    p = mcb.buckets[i].p_first;
    printf ("%d\t%d\t%p\t%p\t%d\t%d\t%d\t%d\t%d\n",
	    i+1, 
	    mcb.buckets[i].bcb_size, 
	    p, 
	    mcb.buckets[i].p_last, 
	    mcb.buckets[i].bcb_in_use,
	    mcb.buckets[i].bcb_free, 
	    mcb.buckets[i].bcb_stats_allocated, 
	    mcb.buckets[i].bcb_stats_free,
	    mcb.buckets[i].bcb_stats_realloc);
#ifdef DEBUG
    printf ("buff\tp_first\tflag\tbyte_count\n");
    for (j=0; j<MAX_BUFFERS; j++) {
      p_buff = (BUFFER_HEADER *) p;
      printf("  %d:\t%p\t%d\t%d\n", j, p_buff, p_buff->flag, p_buff->byte_count);

      p = p + sizeof(BUFFER_HEADER) + mcb.buckets[i].bcb_size;
    }
#endif
  }
  printf("\n");

}

/*
 * Function name: find_bucket_num_by_size
 *
 * Description: Finds the correct bucket based on size
 *              
 *
 * Input: An integer representing the number of bytes needed 
 * 
 * Output: Returns the bucket index 
 *
 */

int find_bucket_num_by_size (int size) {
  int bucket_index;
  int i;
  for(i=0; i<MAX_BUCKETS; ++i) {
    // return if there are free buffers in bucket
    if((size<=mcb.buckets[i].bcb_size) && (mcb.buckets[i].bcb_free != 0)) {  
      bucket_index=i;
      break;
    }
  }
  return bucket_index; 
}

/*
 * Function name: find_bucket_num_by_address 
 *
 * Description: Use strtok to parse an input buffer
 *              A command, a name and a value are separated by spaces
 *              The value is followed by a new line
 *
 * Input: A pointer that points to the beginning of the bucket
 * 
 * Output: Returns the index for the bucket address 
 *
 */

int find_bucket_num_by_address (char *p_malloc) {
  char *p_buff;
  char *p_first;
  char *p_last;
  int  bucket_index=0;
  int  i;
  BCB  *p_bcb;

  p_buff = p_malloc - sizeof(BUFFER_HEADER); //buffer header

  for(i=0; i<MAX_BUCKETS; ++i) {
    // return if there are free buffers in bucket
    p_bcb = &mcb.buckets[i];
    p_first = p_bcb->p_first;
    p_last = p_bcb->p_last;

    if((p_buff>=p_first) && (p_buff<=p_last)) {  
      bucket_index=i;
      break;
    }
  }
  return bucket_index; 
}

/*
 * Function name:   nsh_malloc
 *
 * Description:     Allocates the next available buffer based on 
 *                  storage space(bucket index) and the first free 
 *                  buffer based on the flag int the header 
 *
 * Input:           An integer representing byte size
 * 
 * Output:          A pointer to the newly malloc'd buffer 
 */

char * nshMalloc (int size) {
  int bucket_index;
  char *p; // use this for casting
  BUFFER_HEADER *p_buff;
  int i;
  int flag;
  char *p_malloc = NULL; // return this value 

  bucket_index = find_bucket_num_by_size (size);

  // Search to see if bucket is full
  if (mcb.buckets[bucket_index].bcb_free > 0) {
    p_buff = (BUFFER_HEADER *) mcb.buckets[bucket_index].p_first;

    for (i=0; i<MAX_BUFFERS; i++) {
      p_malloc = (char*) p_buff + sizeof(BUFFER_HEADER);
      flag = p_buff->flag;
      if (flag == FREE) {
	p_buff->flag = IN_USE;
	p_buff->byte_count = size;

	mcb.buckets[bucket_index].bcb_in_use ++;
	mcb.buckets[bucket_index].bcb_free --;
	mcb.buckets[bucket_index].bcb_stats_allocated ++;

	break;
      } 
      p = (char*) p_buff;
      p_buff = (BUFFER_HEADER*)(p + sizeof(BUFFER_HEADER) + mcb.buckets[bucket_index].bcb_size);
    }
  }

  return p_malloc;
}

/*
 * Function name:   nsh_calloc 
 *
 * Description:     Allocates memory with all values set to zero 
 *
 * Input:           An integer num and size 
 * 
 * Output:          Returns a pointer to the calloc'd buffer 
 *
 */

char * nshCalloc (int num, int size) {
  char *p;
  int n;
  n = num*size;

  p = nshMalloc (n);
  if (p != NULL) {
    memset (p, 0, n);
  }

  return p;
}

/*
 * Function name:   nsh_realloc 
 *
 * Description:     Reallocates a buffer by stoage size
 *                  checks if the buffer needs to be moved 
 *                  to a higher stoage bucket and frees the current
 *                  buffer in that case 
 *
 * Input:           A pointer to the beginning of the bucket and 
 *                  an integer representing the size needed for the 
 *                  reallocation
 * 
 * Output:          Returns a pointer the reallocated buffer 
 *
 */

char* nshRealloc (char *p, int size) {
  int bucket_index;
  BUFFER_HEADER *p_buff;
  char *p_malloc;
  char *p_new;

  bucket_index = find_bucket_num_by_address (p);

  // Invalid address
  if (bucket_index == -1) {
    printf("free: invalid address %p\n", p);
    return NULL;
  }

  mcb.buckets[bucket_index].bcb_stats_realloc++;

  p_buff = (BUFFER_HEADER*)(p - sizeof(BUFFER_HEADER)); // p is a char minus 4, cast that address to a buffer_header

  // No reallocation, change byte_count
  if (mcb.buckets[bucket_index].bcb_size > size) {
    p_buff->byte_count = size;
  }
  // Reallocation
  else {   
    p_new = nshMalloc (size);
    if (p_new == NULL) {
      return NULL;
    }

    memcpy(p_new, p, p_buff->byte_count);
    nshFree (p);
    p_buff = (BUFFER_HEADER*) (p_new - sizeof(BUFFER_HEADER));
    p_buff->flag = IN_USE;
    p_buff->byte_count = size;

  }

  p_malloc = (char *)p_buff + sizeof(BUFFER_HEADER); 
  return p_malloc;
}

/*
 * Function name:   nsh_free
 *
 * Description:     Function that changes the flag of a buffer 
 *                  to FREE 
 *
 * Input:           A pointer to the beginning of the buffer storage
 *                  directly after the header 
 * 
 * Output:          Void
 *
 */

void nshFree (char *p) {
  int bucket_index;
  BUFFER_HEADER *p_buff;

  bucket_index = find_bucket_num_by_address (p);

  // Invalid address
  if (bucket_index == -1) {
    printf("free: invalid address %p\n", p);
    return;
  }

  // p is a char minus 4, cast that address to a buffer_header
  p_buff = (BUFFER_HEADER*)(p - sizeof(BUFFER_HEADER)); 

  p_buff->flag = FREE;
  mcb.buckets[bucket_index].bcb_in_use--;
  mcb.buckets[bucket_index].bcb_free++;
  mcb.buckets[bucket_index].bcb_stats_free++;

  return;
}

/*
 * Function:     memdump
 *
 * Description:  Outputs the specified buffer in hex dump format.
 *
 * Input:        msg     - message string to display
 *               buf     - Source buffer
 *               len     - Source buffer size
 *
 * Output:       None
 *
 */

void memdump (char *msg, char *buf, int len) {
  int i;
  int count;
  unsigned char ch;

  if ((msg == NULL) || (buf == NULL) || (len <= 0)) {
    return;
  }

  printf("%s: buf = %p, len = %d\n", msg, buf, len);

  count = 0;

  printf("%p  ", buf+count);

  while (count < len) {
    ch = (unsigned char)*(buf+count);
    printf("%02X ", ch);
    count++;

    if ((count % 16) == 0) {
      printf("\n");
      printf("%p  ", buf+count);
    }
  }
  printf("\n\n");
}
