/*
 * File name: nsh_extern_cmd.h
 *
 * Description: Includes functions for using the where command and executing external commands
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_PATH 50

enum type {READ, WRITE, RUN, DIRECT, TYPE_ERROR};

/*
 * Function name: where
 *
 * Description: Finds the permissions of a certain file
 *
 * Input: The directory to search
 *        The type of file to look for
 *        The name of the file to look for
 *
 * Output: The path of the file if found
 *         Null if not found
 *
 */

char* where (int type, char *file);
void exenew (char *path, char *command, char *arg);

/*
 * Function name: exenew
 *
 * Description: Execute an external command
 *
 * Input: the path of the executable
 *        the command to execute
 *        the command argument
 *
 * Output: None
 *
 */

/*
 * Function: parse_path
 *
 * Definition: Takes a path and then parses the path
 *                into different parts by the seperation symbol
 *
 * Input: a path to be parsed
 *
 * Output: number of parsed elements
 */

int parse_path (char *path, char *path_array[]);

/*
 * Function name: where_command_ui
 *
 * Description: process the where command
 *
 * Input: the type of command to look for
 *        the name of the file to look for
 *
 * Output: The location of the file if found
 *         Null if not found
 *
 */

char* where_command_ui  (char *type, char *file);

/*
 * Function name: extern_command_ui
 *
 * Description: Process and external command
 *
 * Input: Command to execute
 *        Command argument
 *
 * Output: none
 *
 */

void extern_command_ui (char *cmd, char *arg);


