/*
 * File name: nsh_env_alias.c
 *
 * Description: Includes functions that maintain two records.
 *              The first is a list of environment variable records.
 *              The second is a list of alias records.
 *              File has the ability to insert, delete, sort and return records.
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#define MAX_ENTIRE_INPUT 500
#define MAX_BUFF_INDEX   100
#define MAX_CMD_INDEX    100
#define MAX_NAME_INDEX   100
#define MAX_VALUE_INDEX  100

enum COMMANDS {
  EXIT = 1, 
  EXTERN, 
  SET, TES, 
  ALIAS, SAILA, 
  HEAD, NEXT, 
  MEMTEST, MEMDUMP, 
  ECHO, 
  FILEIO, 
  WHERE};

enum ERROR    {ERROR = -1};
enum LISTTYPE {L_ENV = 1, L_ALIAS = 2};

typedef struct rec {
  char variable [MAX_NAME_INDEX];
  char value    [MAX_VALUE_INDEX];
  int command_code;
  struct rec *next;
} REC;

/* 
 * Function: nshInsert
 *
 * Description: Allocates the space and places a record into memory
 *
 * Input: two pointers to the variable and value
 *        and an integer referending which list is used
 *
 * Output: returns 1 if insertion was successful
 */

int nshInsert (char *variable, char *value, int list); 

/* Function: nshRemove
 *
 * Description: Function that removes a variable from memory and the list
 *
 * Input: pointer of a variable
 *
 * Output: none
 */

void  nshRemove  (char *variable, int list);

/* Function: nshFind
 *
 * Description: finds the address of the pointer directly
 *              before a variable
 * Input: A pointer to the variable and a pointer to the
 *        beginning of the list
 *
 * Output: A pointer to the element directly before the variable
 */

char * nshFind (REC **list, char *variable);

/* Function: nshFindAlias
 *
 * Description: finds the address of the pointer directly
 *              before a variable
 *
 * Input: A pointer to the variable and a pointer to the
 *        beginning of the list
 *
 * Output: A pointer to the element directly before the variable
 */

char * nshFindAlias (char *variable);

/* Function: nshFindEnv
 *
 * Description: finds the address of the pointer directly
 *              before a variable
 *
 * Input: A pointer to the variable and a pointer to the
 *        beginning of the list
 *
 * Output: A pointer to the element directly before the variable
 */

char * nshFindEnv (char *variable);

/* Function: nshFindPath
 *
 * Description: Looks for the path environment variable
 *
 * Input: None
 *
 * Output: A character pointer to the path record
 */

char * nshFindPath (void);

/* Function: nshHead
 *
 * Description: Places the "next" pointer to point to the beggining
 *              of the list and calls the next function
 *
 * Input: an integer referencing which list to use
 *
 * Output: prints the head variable and value
 */

void nshHead (int list);

/* Function: nshNext
 *
 * Description: Prints the next variable and moves the next pointer
 *
 * Input: An integer refernecing the list to use
 *
 * Output: prints the next variable and value
 */

void nshNext (int list);

/* Function: nshDisplayList
 *
 * Description: Displays the entire list
 *
 * Input: Integer representing which list to use
 *
 * Output: prints the list
 */

void nshDisplayList (int list);

/* Function: nshDisplayRec
 *
 * Description: displays a record by calling the nshFind function
 *
 * Input: pointer to the variable and integer for the list to use
 *
 * Output: prints the record.
 */

void nshDisplayRec (char *variable, int list);

/* Function: int_command_code
 *
 * Description: Used to express which command to execute
 *
 * Input: pointer to the command typed
 *
 * Output: integer representing a command
 */

int find_command_code (char *variable);

/*
 * Function name: parse_env_buf
 *
 * Description: Removes the first part of a buffer until the ! is reached
 *              Remove that string from the buffer
 *
 * Input:      The string to be parced
 *             A variable to save the string
 *
 * Output:     None
 *
 */

int parse_env_buf (char *buf,  char *new_value);

/*
 * Function name: nshExtend
 *
 * Description: Extend an environment variable by prepending or appending a env variable's value
 *
 * Input: Variable to extend
 *        Value that contains an @ sign followed by a variable name
 *        The variable and value are separated by an ! exclamation mark
 *
 * Output: None
 *
 */

void nshExtend (char *variable, char *value);





