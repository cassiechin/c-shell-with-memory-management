/*
 * File name: nsh_util.c
 *
 * Description: Includes all functions directly related to the shell interface
 *              Includes the shell function as well as processing the input
 *
 * Group Members:  Cassie Chin, Jordan Yankovich
 *
 * Date of Last Modification: December 7, 2013
 *
 */

#include "nsh_env_alias.h"
#include "nsh_malloc.h"
#include "nsh_extern_cmd.h"

//#include "nsh_file.h"

/*
 * Function name: parse_buf
 *
 * Description: Use strtok to parse an input buffer
 *              A command, a name and a value are separated by spaces
 *              The value is followed by a new line
 *
 * Input: A buffer containing a set of strings
 *        A variable to store the command in
 *        A variable to store the name in
 *        A variable to store the value in
 * 
 * Output: Returns 0 if buffer contains 0 strings
 *         Returns 1 if buffer contains only a command
 *         Returns 2 if buffer contains a command and a name
 *         Returns 3 if buffer contains a command, a name and a value
 *
 */

int parse_buf(char *buf, char *cmd, char *variable, char *value) {
  char *cp;
  char tbuf[80];
  int i, counter=0;
  int num_of_args=0;
  
  // copy buf to temporary buffer and initialize parameters
  strcpy(tbuf, buf);         
  cmd      [0] = '\0';           
  variable [0] = '\0';
  value    [0] = '\0';

  // Get command
  cp = strtok (tbuf, " \n\t");
  if (cp != NULL) {          
    strcpy(cmd, cp);           
    num_of_args++;
  }
  // Get alias name
  cp = strtok (NULL, " \n\t");   
  if (cp != NULL) {          
    strcpy(variable, cp);          
    num_of_args++;
  } 
  // Get alias value
  cp = strtok (NULL, "\n");   
   if (cp != NULL) {           
    while (*cp == ' ') {   
      cp++;   
    }

    strcpy(value, cp);     
    num_of_args++;
  }

  return num_of_args;
}

/*
 * Function name: test_for_at
 *
 * Description: test to see if a string contains an @ symbol
 *
 * Input: string to test
 *
 * Output: 1 = True, the @ sign was found
 *         0 = False, the @ sign was not found
 *
 */

int test_for_at (char *value) {
  char *cp;
  int i;
  int len;
  len = strlen(value);
  for (i = 0; i<len; i++) {
    if (value[i] == '@') {
      return 1;  // 1 = True, the @ sign was found
    }
  }
  return 0; // 0 = False, the @ sign was not found
}


/*
 * Function name: test_for_valid_complex_string
 *
 * Description: test to see if a string contains '{}'
 *
 * Input: string to test
 *
 * Output: 1 = True, complex string was found
 *         0 = False, complex string was not found
 *
 */

int test_for_valid_complex_string (char *buf) {
  char *cp;
  int i;
  int len;
  int start = -1;
  int end   = -1;

  len = strlen(buf);

  for (i = 0; i<len; i++) {
    if (buf[i] == '{') start = i;
    if (buf[i] == '}') end = i;
  }

  if (start == -1) return 0; // no complex string found
  if (start < end) return 1; // valid complex string
  if (start > end) return 0; // no right curly brace found;

  return 0; // 0 = False

}

/*
 * Function name: process_buf
 *
 * Description: finds the command code of a buffer and process the value
 *
 * Input: buffer to process
 *
 * Output: 1 if there was an error
 *
 */

int proccess_buf (char * buf) {
  char cmd      [MAX_CMD_INDEX];
  char variable [MAX_NAME_INDEX];
  char value    [MAX_VALUE_INDEX];
  int command_code;
  int num_of_args; // in the input buffer 
  int at_status;
  int cs_status;
  int test_number;
  char *ptr;
  REC *p_rec;
  char *location;
  int is_internal_cmd;

  num_of_args = parse_buf(buf, cmd, variable, value);
  if (num_of_args == 0) return 1;
  
  command_code = find_command_code(cmd);     // Sets the command string to an integer defined by enumerated command setting
  
  if (command_code == ERROR) {
    p_rec = (REC*) nshFindAlias (cmd); // search for command in alias list and execute the value of that command

    if (p_rec != 0 && test_for_valid_complex_string (p_rec->value) == 1) {
      // re enter command
      strcpy (buf, p_rec->value);                          // copy value of alias into a buffer
      memmove (buf, buf+1, strlen(buf));                   // remove the left curly brace
      buf[strlen(buf)-1] = '\0';                           // remove the right curly brace
      num_of_args = parse_buf (buf, cmd, variable, value); // parse the new input buffer
      if (num_of_args == 0) return 1;                        // return if nothing was in the buffer
      command_code = find_command_code (cmd);              // find the new command code
    }
  }
  
  switch (command_code) {
  case (EXIT):  
    exit (1);
    //      return 0;
      
  case (SET): 
    if (num_of_args == 1) nshDisplayList(L_ENV);
    if (num_of_args == 2) nshDisplayRec(variable, L_ENV);
    if (num_of_args == 3) {
      at_status = test_for_at (value);  
      if (at_status == 1) {  // does have an at , extend
	nshExtend (variable, value);
      }
      else { // no at symbol, so set
	nshInsert (variable, value, L_ENV);
      }
    }
    break;

  case (TES):
    nshRemove (variable, L_ENV);
    break;
    
  case (ALIAS):
    if (num_of_args == 1) nshDisplayList (L_ALIAS);
    if (num_of_args == 2) nshDisplayRec (variable, L_ALIAS);
    if (num_of_args == 3) nshInsert (variable, value, L_ALIAS);
    break;
    
  case (SAILA):
    nshRemove (variable, L_ALIAS);
    break;
      
  case (MEMTEST):
    test_number = atoi (variable);
      
    switch (test_number) {
    case 1: memtest1(); break;
    case 2: memtest2(); break;
    case 3: memtest3(); break;
    default: break;
    }
    break;

  case (MEMDUMP):
    dump_mcb();
    break;
    
  case (HEAD):
    if (strcmp("env", variable) == 0) {
      nshHead (L_ENV);
      break;
    }
    if (strcmp("alias", variable) == 0) { 
      nshHead (L_ALIAS);
      break;
    }
    
  case (NEXT):
    if (strcmp("env", variable) == 0) {
      nshNext  (L_ENV);
      break;
    }
    if (strcmp("alias", variable) == 0) {
      nshNext (L_ALIAS);
      break;
    }
    
  case (ECHO):
    strcat (variable, " ");  
    strcat (variable, value);
    printf("%s\n", variable);
    break;

  case (FILEIO):
    execute_resource_file (variable); // input resource file
    return 0;
    break;

  case (WHERE):
    location = where_command_ui (variable, value);
    if (location != NULL) printf("%s\n", location);
    break;
    
  default:
    buf[strlen(buf)-1] = '\0'; // replace "\n" with a null terminator
    num_of_args = parse_buf (buf, cmd, variable, value); // parse the new input buffer
    if (num_of_args == 1)  extern_command_ui (cmd, NULL);
    else extern_command_ui (cmd, variable);
    // printf("-nsh: %s: command not found\n", cmd);
  }
  
  return 1;
}

/*
 * Function Name: shell
 *
 * Description: Read a line from stdin into a buffer string
 *              Parse that string into a command, a name and a value
 *              A switch statement processes th command 
 *
 * Input: None
 *
 * Output: None 
 *
 */

void shell (void) {
  struct passwd *pw = getpwuid( getuid() );
  const char *homedir = pw->pw_dir;
  char file [100] = "";

  int rc;
  char buf      [MAX_BUFF_INDEX];
  char entire_input [300] = ""; 
  int exit_status = 1;
  int dollar_return = 0;

  nsh_malloc_init(); // Initialize memory subsystem used by the shell

  strcpy (file, homedir);
  strcat (file, "/" );
  strcat (file, ".nshrc");      // add file name to the directory (home) that it's in
  rc = execute_resource_file (file); // input resource file

  // Set the home directory if the resource file does not exist
  if (rc == 0) nshInsert ("home", (char *) homedir, L_ENV);

  dollar_return = 0;

  while (1) {
    if (dollar_return == 0) printf("nsh>> "); 
    fgets(buf, sizeof(buf), stdin);  // read a line from stdin into a buffer string   
    if (buf[0] == '\n') continue;

    dollar_return = remove_dollar_sign (buf);
    remove_at_sign (buf);
    strcat (entire_input, buf);
    if (dollar_return == 0) {
      parse_file_lines (entire_input);
      memset (entire_input, 0, strlen (entire_input));
    }
    memset(buf, 0, sizeof(buf));     // CLEAR INPUT BUFFERS
  }
}
